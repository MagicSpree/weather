package bootstrap;

import api.NetworkResponse;
import model.Weather;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Bootstrap {

    final String API_KEY = "0f81d2effae4e0d27883df3f11aeefbd";
    final String CITY = "Penza";

    public void run() {
        NetworkResponse networkResponse = new NetworkResponse(API_KEY);
        Call<Weather> weatherCall = networkResponse.getInfoWeather(CITY);
        weatherCall.enqueue(new Callback<>() {

            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                if (response.isSuccessful()) {
                    print(response.body().toString());
                } else {
                    int codeError = response.code();
                    String responseErrorMessage = response.message();
                    String messageError = String.format("Ошибка при получении ответа от сервера! Код ошибки: %d, Сообщение: %s", codeError, responseErrorMessage);
                    print(messageError);
                }
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                print(t.getMessage());
            }
        });
    }

    private void print(String message) {
        System.out.println(message);
    }
}
