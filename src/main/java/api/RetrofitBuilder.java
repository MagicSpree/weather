package api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    /**
     * Создание объекта Retrofit
     * @return объект Retrofit
     */
    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiService getService() {
        return getRetrofit().create(ApiService.class);
    }
}
