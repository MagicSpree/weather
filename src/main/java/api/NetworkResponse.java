package api;

import model.Weather;
import retrofit2.Call;

public class NetworkResponse {

    private final String apiKey;

    public NetworkResponse(String apiKey) {
        this.apiKey = apiKey;
    }

    public Call<Weather> getInfoWeather(String city) {
        return RetrofitBuilder.getService().getWeather(city, apiKey);
    }
}
