package api;

import model.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    /**
     * Формируем тело запроса
     * @param city Название города
     * @param api ключ для получения данных через API
     *
     * @return Вызов внутри которого находится нужный объект класса
     */
    @GET("weather?units=metric")
    Call<Weather> getWeather(
            @Query("q") String city,
            @Query("appid") String api
    );
}
