package model;

import com.google.gson.annotations.JsonAdapter;
import deserializer.WeatherDeserializer;

import java.util.Date;

@JsonAdapter(WeatherDeserializer.class)
public class Weather {

    private long temp;
    private long humidity;
    private String weatherCondition;
    private String city;
    private String dateWeather;
    private String timeWeather;
    private double wind;
    private double precipitation = 0;


    public Weather() {
        initDate();
    }

    private void initDate() {
        Date dateNow = new Date();
        dateWeather = String.format("%1$td %1$tB %1$tY", dateNow);
        timeWeather = String.format("%1$tR", dateNow);
    }

    public void setTemp(Double temp) {
        this.temp = Math.round(temp);
    }

    public void setHumidity(Double humidity) {
        this.humidity = Math.round(humidity);
    }

    public void setWeatherCondition(String weatherCondition) {
        this.weatherCondition = weatherCondition;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setWind(double wind) {
        this.wind = wind;
    }

    public void setPrecipitation(double precipitation) {
        this.precipitation = precipitation;
    }

    @Override
    public String toString() {
        return "Город: " + city + "\n" +
                "Дата: " + dateWeather + "\n" +
                "Время: " + timeWeather + "\n" +
                "Погодные условия: " + weatherCondition + "\n" +
                "Температура: " + temp + "ºС" + "\n" +
                "Скорость ветра: " + wind + "м/с" + "\n" +
                "Осадки: " + precipitation + "мм" + "\n" +
                "Влажность: " + humidity + "%" + "\n";
    }
}
