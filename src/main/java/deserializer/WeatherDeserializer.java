package deserializer;

import com.google.gson.*;
import model.Weather;
import retrofit2.Retrofit;

import java.lang.reflect.Type;

/**
 * Класс для десериализации (парсинга) из json формата в объект
 */
public class WeatherDeserializer implements JsonDeserializer<Weather> {

    @Override
    public Weather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        Weather weather = new Weather();

        weather.setCity(jsonObject.get("name").getAsString()); //Получаем название города

        JsonArray weatherCondition = jsonObject.getAsJsonArray("weather");
        JsonElement weatherElem = weatherCondition.get(0);
        weather.setWeatherCondition(weatherElem.getAsJsonObject().get("main").getAsString()); //Получаем погоду (солнце, облачно и тд...)


        JsonObject main = jsonObject.get("main").getAsJsonObject();
        weather.setTemp(main.get("temp").getAsDouble()); //Получаем температуру
        weather.setHumidity(main.get("humidity").getAsDouble()); //Получаем влажность

        JsonObject wind = jsonObject.get("wind").getAsJsonObject();
        weather.setWind(wind.get("speed").getAsDouble()); //Получаем скорость ветра

        if (jsonObject.has("rain")) { //Идет ли дождь
            JsonObject rain = jsonObject.get("rain").getAsJsonObject();
            weather.setPrecipitation(rain.get("1h").getAsDouble()); //Получаем осадки
        }
        else {
            if (jsonObject.has("snow")) { //Идет ли снег
                JsonObject rain = jsonObject.get("snow").getAsJsonObject();
                weather.setPrecipitation(rain.get("1h").getAsDouble()); //Получаем осадки
            }
        }

        return weather;
    }
}
